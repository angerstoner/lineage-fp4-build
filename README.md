Automated LineageOS 18.1 builds

## Download and run 
`curl 'https://gitlab.com/angerstoner/lineage-fp4-build/-/raw/main/build.sh' | bash`

## Script Steps
1. Install required packages
2. Cloning build repo
3. Install Android platform tools
4. Install repo tool
5. Setting up git
6. Setting up CCACHE
7. Initializing repo tool
8. Download sources with repo
9. Copy device config (roomservice.xml)
10. Prepare the device-specific code
11. Clone proprietary blobs
12. Breakfast again
13. Change system partition reserved space
14. Build


## OpenGapps
Not included, download from opengapps.org and flash afterwards.

## Credits
FP4 Proprietary blobs:  
- g4bwy (https://github.com/g4bwy/proprietary-vendor_fairphone_FP4)  
- xblac (https://github.com/xblax/android_vendor_fairphone_FP4)

Build Instructions: Error (https://forum.fairphone.com/t/building-lineageos-for-fp4/81461/5)  
LineageOS (https://wiki.lineageos.org/devices/FP3/build)  
OpenGApps (https://github.com/opengapps/aosp_build)  
/e/ (https://doc.e.foundation/support-topics/build-e)  
docker-lineage-cicd (https://github.com/lineageos4microg/docker-lineage-cicd)  