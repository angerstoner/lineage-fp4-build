#!/usr/bin/env bash

echo "### CLONING BUILD REPO ###"
git clone https://gitlab.com/angerstoner/lineage-fp4-build

echo "### INSTALLING PLATFORM TOOLS ###"
wget "https://dl.google.com/android/repository/platform-tools-latest-linux.zip"
unzip platform-tools-latest-linux.zip -d ~
echo 'if [ -d "$HOME/platform-tools" ] ; then
  PATH="$HOME/platform-tools:$PATH"
fi' >> ~/.profile
source ~/.profile

echo "### INSTALLING REPO ###"
mkdir -p ~/bin
mkdir -p ~/android/lineage
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
echo 'if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
fi' >> ~/.profile
source ~/.profile

echo "### SETTING UP GIT ###"
git config --global user.email "angerstoner@mailbox.org"
git config --global user.name "Angerstoner"

echo "### SETTING UP CCACHE ###"
export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache
ccache -M 50G
ccache -o compression=true

echo "### INITIALIZING REPO ###"
cd ~/android/lineage
( yes||: ) | repo init -u https://github.com/LineageOS/android.git -b lineage-18.1

echo "### DOWNLOADING SOURCES WITH REPO ###"
echo "This will take a long while (downloading about 120GB)"
repo sync

echo "### SETUP ROOMSERVICE (DEVICE CONFIG) ###"
mkdir -p .repo/local_manifests/
cp ~/lineage-fp4-build/roomservice.xml .repo/local_manifests/

echo "### BREAKFAST ###"
source build/envsetup.sh
breakfast FP4

echo "### ANOTHER REPO SYNC (for FP4 sources)"
repo sync

echo "### EXTEND SYSTEM PARTION SIZE (in config) ###"
sed -i 's/BOARD_SYSTEMIMAGE_PARTITION_RESERVED_SIZE:= [0-9]*/BOARD_SYSTEMIMAGE_PARTITION_RESERVED_SIZE:= 1073741824/' device/fairphone/FP4/BoardConfig.mk

echo "### BUILD ###"
croot
make -j6 target-files-package otatools

echo "### SIGNING BUILD ###"
croot
make dist
sign_target_files_apks -o --default_key_mappings ~/.android-certs out/dist/*-target_files-*.zip signed-target_files.zip

echo "### CREATE FLASHABLE ZIP ###"
ota_from_target_files -k ~/.android-certs/releasekey signed-target_files.zip signed-ota_update.zip