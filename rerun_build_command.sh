#!/usr/bin/env bash

cd ~/android/lineage
source ~/.profile

echo "### SETTING UP CCACHE ###"
export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache
ccache -M 50G
ccache -o compression=true

echo "### BREAKFAST ###"
source build/envsetup.sh
breakfast FP4

echo "### EXTEND SYSTEM PARTION SIZE (in config) ###"
sed -i 's/BOARD_SYSTEMIMAGE_PARTITION_RESERVED_SIZE:= [0-9]*/BOARD_SYSTEMIMAGE_PARTITION_RESERVED_SIZE:= 1073741824/' device/fairphone/FP4/BoardConfig.mk

echo "### BUILD ###"
croot
make -j6 target-files-package otatools

echo "### SIGNING BUILD ###"
croot
make dist
sign_target_files_apks -o --default_key_mappings ~/.android-certs out/dist/*-target_files-*.zip signed-target_files.zip

echo "### CREATE FLASHABLE ZIP ###"
ota_from_target_files -k ~/.android-certs/releasekey signed-target_files.zip signed-ota_update.zip